pragma solidity ^0.4.24;
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Burnable.sol";
import './Freezable.sol';
import './TimeLockable.sol';

contract BLUCON is ERC20Burnable, Freezable, TimeLockable
{
    string  public  constant name       = "BLUCON";
    string  public  constant symbol     = "BEP";
    uint8   public  constant decimals   = 18;
    
    event Burn(address indexed _burner, uint256 _value);

    constructor( address _registry, uint256 _totalTokenAmount ) public
    {
        _mint(_registry, _totalTokenAmount);
        addSuperAdmin(_registry);
    }

    function transfer(address _to, uint256 _value) public validateAddress(_to) isNotTimeLocked(_to) isNotFrozen(_to) returns (bool) 
    {
        return super.transfer(_to, _value);
    }

    function transferFrom(address _from, address _to, uint256 _value) public validateAddress(_to) isNotTimeLockedFrom(_from, _to) isNotFrozenFrom(_from, _to) returns (bool) 
    {
        return super.transferFrom(_from, _to, _value);
    }

    function approve(address _spender, uint256 _value) public validateAddress(_spender) isNotFrozen(_spender) isNotTimeLocked(_spender) returns (bool) 
    {
        return super.approve(_spender, _value);
    }

    function increaseAllowance( address _spender, uint256 _addedValue ) public validateAddress(_spender) isNotFrozen(_spender) isNotTimeLocked(_spender) returns (bool)
    {
        return super.increaseAllowance(_spender, _addedValue);
    }

    function decreaseAllowance(address _spender, uint256 _subtractedValue) public validateAddress(_spender) isNotFrozen(_spender) isNotTimeLocked(_spender) returns (bool)
    {
        return super.decreaseAllowance(_spender, _subtractedValue);
    }
}   

