// Lee, July 29, 2018
pragma solidity ^0.4.24;
import "./Whitelistable.sol";

contract TimeLockable is Whitelistable {
    mapping (address => uint256) public timelockedAccounts;
    event LockedFunds(address indexed target, uint256 timelocked);

    modifier isNotTimeLocked( address _addr ) {
        require( whitelistedTransferer[_addr] || (now >= timelockedAccounts[msg.sender]));
        _;
    }

    modifier isNotTimeLockedFrom( address _from, address _to) {
        require( whitelistedTransferer[_to] || ( now >= timelockedAccounts[_from] && now >= timelockedAccounts[msg.sender]));
        _;
    }
    
    function timeLockAccount(address _target, uint256 _releaseTime) public onlySuperAdmins validateAddress(_target) {
        require(_releaseTime > now);
        require(timelockedAccounts[_target] != _releaseTime);

        timelockedAccounts[_target] = _releaseTime;
        emit LockedFunds(_target, _releaseTime);
    }
}
