
const { assertRevert } = require('./helpers/assertRevert');
const TimeLockable = artifacts.require('TimeLockable');
const BigNumber = web3.BigNumber;

require('chai')
  .use(require('chai-bignumber')(BigNumber))
  .should();


contract('TimeLockable', (accounts) => {
	beforeEach(async function () {
		this.timelockable = await TimeLockable.new({from: accounts[0]});
	});

	describe('timeLockAccount', () => {

		it('contract owner can add a time-locked account', async function () {
			await this.timelockable.timeLockAccount(accounts[1], 2550717408, {from: accounts[0]});
			(await this.timelockable.timelockedAccounts(accounts[1])).should.be.bignumber.equal(new BigNumber(2550717408));
		});

		it('a non super admin can not time-lock another account', async function () {
			// this should fail because accounts[1] is not a super admin
			await assertRevert(
				this.timelockable.timeLockAccount(accounts[2], 2550717408, {from: accounts[1]})
			);
		});

		it('cannot add a time-locked account that is already a time-locked account with same timestamp', async function () {

		  	await this.timelockable.timeLockAccount(accounts[1], 2550717408, {from: accounts[0]});
		  	(await this.timelockable.timelockedAccounts(accounts[1])).should.be.bignumber.equal(new BigNumber(2550717408));
		 	// second call fails
			await assertRevert(
				this.timelockable.timeLockAccount(accounts[1], 2550717408, {from: accounts[0]})
			);
		});

		it('cannot add a time-locked account because of wrong timestamp', async function () {

			await assertRevert(
				this.timelockable.timeLockAccount(accounts[1], 1550717408, {from: accounts[0]})
			);
		});


	});
});