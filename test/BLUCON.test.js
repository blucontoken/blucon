const { assertRevert } = require('./helpers/assertRevert');
const BLUCON = artifacts.require('BLUCON');
const BigNumber = web3.BigNumber;
const INITIAL_SUPPLY = new BigNumber(2000000000).mul(1000000000000000000);
const INITIAL_SUPPLY2 = INITIAL_SUPPLY.add(100);

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

 const ADVISOR_LOCKUP_END = new BigNumber(1551398399);
 const TEAM_LOCKUP_END = new BigNumber(1567295999);

require('chai')
  .use(require('chai-bignumber')(BigNumber))
  .should();

contract('BLUCON', (accounts) => {
  beforeEach(async function () {
    this.crn = await BLUCON.new(
      // set accounts[1] to be registry
      accounts[1], INITIAL_SUPPLY
    );
  });

  describe('constructor', () => {
    it('validate token minting', async function () {
      // accounts[0] should be the owner
      (await this.crn.owner()).should.be.equal(accounts[0]);

      // total supply should be initial supply
      (await this.crn.totalSupply()).should.be.bignumber.equal(INITIAL_SUPPLY);

      // balance of register (accounts[1]) should be initial_supply
      (await this.crn.balanceOf(accounts[0])).should.be.bignumber.equal(0);
      (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);

      // accounts[1] should be a super admin
      (await this.crn.superAdmins(accounts[0])).should.be.equal(false);
      (await this.crn.superAdmins(accounts[1])).should.be.equal(true);
      (await this.crn.superAdmins(accounts[2])).should.be.equal(false);
    });
  });

  describe('transfer', () => {
    it('simple transfer case should succeed and change balance', async function () {
      // transfer tokens from registry (accounts[1]) to another account
      (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);

      // perform transfer
      await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});

      // balances should be updated
      (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY.sub(5000));
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));
    });

    it('transferFrom case should succeed and change balance', async function () {
      // transfer tokens from registry (accounts[1]) to another account
      (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);

      // perform transfer
      await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));


      const spender = accounts[3];
      await this.crn.approve(spender, 1000, { from: accounts[2] });
      (await this.crn.allowance(accounts[2], spender)).should.be.bignumber.equal(1000);

      await this.crn.transferFrom(accounts[2], accounts[3], 1000, { from: spender });

      // balances should be updated
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(4000));
    });

    it('transfer fails when token is frozen', async function () {
      // freeze accounts[1]
      await this.crn.freezeToken(true, {from: accounts[1]});
      (await this.crn.frozenToken()).should.be.equal(true);

      // perform transfer
      await assertRevert(this.crn.transfer(accounts[2], 5000, {from: accounts[1]}));
    });


    it('transfer fails if sender is frozen', async function () {
      // freeze accounts[1]
      await this.crn.freezeAccount(accounts[1], true, {from: accounts[1]});

      // perform transfer
      await assertRevert(this.crn.transfer(accounts[2], 5000, {from: accounts[1]}));
    });



    // added by lee
    it('transfer should be succeed if sender is frozen and receiver is whitelisted', async function () {
      // freeze accounts[1]
      await this.crn.freezeAccount(accounts[1], true, {from: accounts[1]});
      await this.crn.setWhitelistedTransferer(accounts[2], true, {from: accounts[0]});
      await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
      // perform transfer
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));
    });



    it('transfer should be succeed if sender is frozen and receiver is frozen and whitelisted.', async function () {
      // freeze accounts[1]
      await this.crn.freezeAccount(accounts[1], true, {from: accounts[1]});
      await this.crn.freezeAccount(accounts[3], true, {from: accounts[0]});
      await this.crn.setWhitelistedTransferer(accounts[3], true, {from: accounts[0]});
      // (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(0));
      (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
      
      await this.crn.transfer(accounts[3], 5000, {from: accounts[1]});
      (await this.crn.balanceOf(accounts[3])).should.be.bignumber.equal(new BigNumber(5000));      
    });


    it('approve should be failed if sender is frozen', async function () {
        (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);
        await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));

        await this.crn.freezeAccount(accounts[2], true, {from: accounts[0]});
        const spender = accounts[3];
        await assertRevert(this.crn.approve(spender, 1000, { from: accounts[2] }));
    });



    it('approve should be failed if spender is frozen', async function () {
        (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);
        await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));

        await this.crn.freezeAccount(accounts[3], true, {from: accounts[0]});
        const spender = accounts[3];
        await assertRevert(this.crn.approve(spender, 1000, { from: accounts[2] }));
    });


    it('approve should be failed if sender is frozen', async function () {
        (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);
        await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));

        await this.crn.setWhitelistedTransferer(accounts[2], true, {from: accounts[0]});
        await this.crn.freezeAccount(accounts[2], true, {from: accounts[0]});
        const spender = accounts[3];
        await assertRevert(this.crn.approve(spender, 1000, { from: accounts[2] }));
    });


    it('approve should be succeed if sender is frozen and whitelisted', async function () {
        (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);
        await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));

        await this.crn.setWhitelistedTransferer(accounts[3], true, {from: accounts[0]});
        await this.crn.freezeAccount(accounts[3], true, {from: accounts[0]});
        const spender = accounts[3];
        this.crn.approve(spender, 1000, { from: accounts[2] });
    });


    it('approve and transfrom should be failed if sender is frozen and whitelisted', async function () {
       // transfer tokens from registry (accounts[1]) to another account
      (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);

      // perform transfer
      await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));


      const spender = accounts[3];
      await this.crn.approve(spender, 1000, { from: accounts[2] });
      (await this.crn.allowance(accounts[2], spender)).should.be.bignumber.equal(1000);


      await this.crn.freezeAccount(accounts[3], true, {from: accounts[0]});
      await assertRevert(this.crn.transferFrom(accounts[2], accounts[3], 1000, { from: spender }));

      // balances should be updated
      // (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(4000));

    });



    // added by lee
    it('transfer should be failed if sender is already timelocked', async function () {
      // freeze accounts[1]
      await this.crn.timeLockAccount(accounts[1], 2550717408, {from: accounts[0]});
       await assertRevert(this.crn.timeLockAccount(accounts[1], 2550717408, {from: accounts[0]}));
    });

    it('transfer should be failed if sender is already freezed', async function () {
      // freeze accounts[1]
      await this.crn.freezeAccount(accounts[2], true, {from: accounts[0]});
       await assertRevert(this.crn.freezeAccount(accounts[2], true, {from: accounts[0]}));
    });



    it('transfer should be failed if sender is already whitelisted', async function () {
      // freeze accounts[1]
      await this.crn.setWhitelistedTransferer(accounts[2], true, {from: accounts[0]});
       await assertRevert(this.crn.setWhitelistedTransferer(accounts[2], true, {from: accounts[0]}));
    });


    // added by lee
    it('transfer should be succeed if sender is timelocked and receiver is whitelisted', async function () {
      // freeze accounts[1]
      await this.crn.timeLockAccount(accounts[1], 2550717408, {from: accounts[0]});
      await this.crn.setWhitelistedTransferer(accounts[2], true, {from: accounts[0]});
      await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
      // perform transfer
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));
    });

    it('transfer should be succeed if sender is timelocked and receiver is timelocked and whitelisted.', async function () {
      // freeze accounts[1]
      await this.crn.timeLockAccount(accounts[1], 2550717408, {from: accounts[0]});
      await this.crn.timeLockAccount(accounts[3], 2550717408, {from: accounts[0]});
      await this.crn.setWhitelistedTransferer(accounts[3], true, {from: accounts[0]});
      // (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(0));
      (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
      
      await this.crn.transfer(accounts[3], 5000, {from: accounts[1]});
      (await this.crn.balanceOf(accounts[3])).should.be.bignumber.equal(new BigNumber(5000));      
    });


    it('approve should be failed if sender is timelocked', async function () {
        (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);
        await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));

        await this.crn.timeLockAccount(accounts[2], 2550717408, {from: accounts[0]});
        const spender = accounts[3];
        await assertRevert(this.crn.approve(spender, 1000, { from: accounts[2] }));
    });



    it('approve should be failed if spender is timelocked', async function () {
        (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);
        await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));
        await this.crn.timeLockAccount(accounts[3], 2550717408, {from: accounts[0]});
        const spender = accounts[3];
        this.crn.approve(spender, 1000, { from: accounts[2] });
    });


    it('approve should be succeed if sender is timelocked', async function () {
        (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);
        await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));
        await this.crn.setWhitelistedTransferer(accounts[2], true, {from: accounts[0]});
        await this.crn.timeLockAccount(accounts[2], 2550717408, {from: accounts[0]});
        const spender = accounts[3];
        await assertRevert(this.crn.approve(spender, 1000, { from: accounts[2] }));
    });


    it('approve should be succeed if sender is timelocked and whitelisted', async function () {
        (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);
        await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
        (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));

        await this.crn.setWhitelistedTransferer(accounts[3], true, {from: accounts[0]});
        await this.crn.timeLockAccount(accounts[3], 2550717408, {from: accounts[0]});
        const spender = accounts[3];
        this.crn.approve(spender, 1000, { from: accounts[2] });
    });


    it('approve and transfrom should be failed if sender is frozen and whitelisted', async function () {
       // transfer tokens from registry (accounts[1]) to another account
      (await this.crn.balanceOf(accounts[1])).should.be.bignumber.equal(INITIAL_SUPPLY);
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);

      // perform transfer
      await this.crn.transfer(accounts[2], 5000, {from: accounts[1]});
      (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(5000));


      const spender = accounts[3];
      await this.crn.approve(spender, 1000, { from: accounts[2] });
      (await this.crn.allowance(accounts[2], spender)).should.be.bignumber.equal(1000);


      await this.crn.timeLockAccount(accounts[2], 2550717408, {from: accounts[0]});

      await this.crn.timeLockAccount(accounts[3], 2550717408, {from: accounts[0]});
      await assertRevert(this.crn.transferFrom(accounts[2], accounts[3], 1000, { from: spender }));

      // balances should be updated
      // (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(new BigNumber(4000));

    });













    it('transfer fails if destination is frozen', async function () {
      // freeze accounts[1]
      await this.crn.freezeAccount(accounts[2], true, {from: accounts[1]});

      // perform transfer
      await assertRevert(this.crn.transfer(accounts[2], 5000, {from: accounts[1]}));
    });

    it('transfer fails if destination is not a valid address', async function () {
      /* TODO */
      
      await assertRevert(
        this.crn.transfer(ZERO_ADDRESS, 100, {from: accounts[2]})
      );
    });

    it('when the spender is the frozen account', async function () {
        const amount = 100;
        const spender = accounts[2];
        await this.crn.freezeAccount(accounts[1], true, {from: accounts[0]});
        // this should fail because accounts[1] is frozen account
        await assertRevert(this.crn.approve(spender, amount, { from: accounts[1] }));
    });

    it('when the spender is the frozen token', async function () {
        const amount = 100;
        const spender = accounts[2];
        // this should fail because accounts[1] is frozen account
        await this.crn.freezeToken(true, {from: accounts[0]});
        (await this.crn.frozenToken()).should.be.equal(true);

        await assertRevert(this.crn.approve(spender, amount, { from: accounts[1] }));
        await assertRevert(this.crn.transferFrom(accounts[1], accounts[3], amount, { from: spender }));


    });

    it('when the spender is the frozen account(message sender)', async function () {
        const amount = 100;
        const spender = accounts[2];

        await this.crn.freezeAccount(accounts[1], true, {from: accounts[0]});
        await assertRevert(this.crn.approve(spender, amount, { from: accounts[1] }));
        await assertRevert(this.crn.transferFrom(accounts[1], accounts[3], amount, { from: spender }));
    });

    it('increase allowance', async function () {
        const amount = 100;
        const spender = accounts[2];
        const increasedAmount = 200;

        await this.crn.approve(spender, amount, { from: accounts[1] });
        (await this.crn.allowance(accounts[1], spender)).should.be.bignumber.equal(amount);


        await this.crn.increaseAllowance(spender, amount, { from: accounts[1] });
        (await this.crn.allowance(accounts[1], spender)).should.be.bignumber.equal(increasedAmount);
    });

    it('decrease allowance', async function () {
        const amount = 100;
        const spender = accounts[2];
        const decreasedAmount = 50;

        await this.crn.approve(spender, amount, { from: accounts[1] });
        (await this.crn.allowance(accounts[1], spender)).should.be.bignumber.equal(amount);

        await this.crn.decreaseAllowance(spender, decreasedAmount, { from: accounts[1] });
        (await this.crn.allowance(accounts[1], spender)).should.be.bignumber.equal(decreasedAmount);
    });

    it('change owner and add super admin', async function () {
        await this.crn.transferOwnership(accounts[2]);
        (await this.crn.owner()).should.be.equal(accounts[2]);
        await this.crn.addSuperAdmin(accounts[3], { from: accounts[2] });
        (await this.crn.superAdmins(accounts[3])).should.be.equal(true);

        await this.crn.removeSuperAdmin(accounts[3], { from: accounts[2] });
        (await this.crn.superAdmins(accounts[3])).should.be.equal(false);
        await assertRevert( this.crn.addSuperAdmin(accounts[3], { from: accounts[5] }));
    });





    // it('minting....', async function () {
    //     (await this.crn.totalSupply()).should.be.bignumber.equal(INITIAL_SUPPLY);
    //     (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(0);
    //     await this.crn.mint(accounts[2], 100, { from: accounts[0] });
    //     (await this.crn.balanceOf(accounts[2])).should.be.bignumber.equal(100);

    //     (await this.crn.totalSupply()).should.be.bignumber.equal(INITIAL_SUPPLY2);

    //     // await assertRevert( this.crn.mint(accounts[2], { from: accounts[2] }));
    //     // await console.log(this.crn.totalSupply());

    // });

    // it('minting, must be failed.', async function () {
    //     // await this.crn.mint(accounts[2], 100, { from: accounts[0] });
    //     await assertRevert( this.crn.mint(accounts[3], 100, { from: accounts[2] }));
    //     // await console.log(this.crn.totalSupply());

    // });


  });
});