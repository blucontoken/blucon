var Token = artifacts.require("./BLUCON.sol");

var tokenContract;

module.exports = function(deployer) {
    var admin = "0x53A4f93B2a36B033Cdc16F9830CDdF8Bd640864f"; 
    var totalTokenAmount = 3 * 1000000000 * 1000000000000000000;
    return Token.new(admin, totalTokenAmount).then(function(result) {
        tokenContract = result;
    });
};
